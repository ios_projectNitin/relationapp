//
//  SettingsViewController.swift
//  Relationship
//
//  Created by user on 26/06/18.
//  Copyright © 2018 e0000068. All rights reserved.
//

import UIKit

class SettingsViewController: BaseClassViewController {

    @IBOutlet weak var lblName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
