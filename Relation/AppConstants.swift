//
//  AppConstants.swift
//  FansKick
//
//  Created by FansKick-Nitin on 11/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit


let DATE_FORMAT = "MMM dd, yyyy"
let DATE_TIME_FORMAT = "MMM dd, YYYY hh:mm a"

let logOutTitle         = "Log out"
let logOutSubTitle      = "Are you sure you wish to log out?"
let fanskickFontIcons   = "fanskick-font-icon-set"

//@@@ NSNotificationCenter

//@@@ Segues

let logInVCToTabbarControllerSegue  = "logInVCToTabbarController"

let emailMaxLength                  =       60
let passwordMaxLength               =       20
let subjectMaxLength                =       90
let addressSniptMaxLength           =       32

let passwordMinLength               =       6
let nameMaxLength                   =       60
let phoneNumberMaxLength            =       15
let institueNameMaxLength           =       60
let feedbackMaxLength               =       2000
let addressMaxLength                =       120
let referralMaxLength               =       12

//@@@ Validation strings

let noFixtures                     = "No upcoming fixture found for this filter, Please change filter criteria."
let blankEmail                      = "Please enter email."
let invalidEmail                    = "Please enter valid email."
let blankPassword                   = "Please enter password."
let validEmailPassword              = "Please enter valid Email/Mobile number"
let minPassword                     = "Password must be at least 8 characters long."
let blankName                       = "Please enter your name."
let blankFirstName                  = "Please enter first name."
let blankLastName                   = "Please enter last name."
let blankMobileNumber               = "Please enter phone number."
let invalidMobileNumber             = "Please enter valid phone number."
let blankCurrentPassword            = "Please enter current password."
let blankNewPassword                = "Please enter new password."
let blankRePassword                 = "Please re-type new password."
let minCurrentPassword              = "Please enter valid password."
let minNewPassword                  = "Please enter valid new password."
let minRePassword                   = "Please re-type valid new password."
let mismatchNewAndRePassword        = "Passwords do not match"
let mismatchPassowrdAndConfirmPassword = "Confirm password must match with password."

//Success
let forgotPasswordSuccess = "An email has been sent to your email address. Follow the directions in the email to reset your password."


class AppConstants: NSObject {

}
