//
//  BaseClassViewController.swift
//  Relationship
//
//  Created by user on 26/06/18.
//  Copyright © 2018 e0000068. All rights reserved.
//

import UIKit

class BaseClassViewController: UIViewController ,DrawerControllerDelegate{

    var drawerVw = DrawerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false

        // Do any additional setup after loading the view.
    }

    func pushTo(selectedRow: Int) {
        switch selectedRow {
        case 0:
            let controller = PushScreen.MyProfileViewController
            controller?.hidesBottomBarWhenPushed = true
            self.PushToScreen(screen: controller!)
        case 1:
            self.tabBarController?.selectedIndex = 2
        case 2:
            self.tabBarController?.selectedIndex = 1
        case 3:
            self.tabBarController?.selectedIndex = 4
        case 4:
            self.tabBarController?.selectedIndex = 3
        case 5:
            self.tabBarController?.selectedIndex = 0
        default:
            debugPrint("")
        }
    }
    
    @IBAction func actShowMenu(_ sender: Any) {
        drawerVw = DrawerView(aryControllers:DrawerArray.array, isBlurEffect:false, isHeaderInTop:true, controller:self)
        drawerVw.delegate = self
        
        //**** OPTIONAL ****//
        // Can change GradientColor of background view, text color, user name text color, font, username
        drawerVw.changeGradientColor(colorTop: UIColor.groupTableViewBackground, colorBottom: UIColor.white)
        drawerVw.changeCellTextColor(txtColor: UIColor.black)
        drawerVw.changeUserNameTextColor(txtColor: UIColor.white)
        drawerVw.changeFont(font: UIFont(name:"Avenir Next", size:18)!)
        drawerVw.changeUserName(name: "Nitin Agnihotri")
        drawerVw.show()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
