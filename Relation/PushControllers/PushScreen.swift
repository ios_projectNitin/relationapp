//
//  Controller.swift
//  Relationship
//
//  Created by MSS Softprodigy on 26/06/18.
//  Copyright © 2018 Harish. All rights reserved.
//

import Foundation
import UIKit

enum PushScreen {
    
    static var MyProfileViewController : MyProfileViewController! { return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController }
}

// MARK:- UIViewController

//xcode - Device support files for iOS 12.0 (16A5288q)
extension UIViewController
{
    func PushToScreen(screen:UIViewController){
        self.navigationController?.pushViewController(screen, animated: true)
    }
}




