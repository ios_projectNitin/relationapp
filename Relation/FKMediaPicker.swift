//
//  FKMediaPicker.swift
//  FansKick
//
//  Created by FansKick-Nitin on 13/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMediaPicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    static let mediaPicker = FKMediaPicker()
    
    typealias DidFinishPickingMediaBlock = (_ info: [String : Any], _ pickedImage: UIImage?) -> Void
    private var finishedPickingMediaWithInfo: DidFinishPickingMediaBlock?
    
    typealias DidCancelledPickingMediaBlock = () -> Void
    var cancelledPickingMediaBlock: DidCancelledPickingMediaBlock?
    
    func pickMediaFromCamera(cameraBlock: @escaping DidFinishPickingMediaBlock) ->Void {
        
        finishedPickingMediaWithInfo = cameraBlock
        
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            
            if let currentController = UIWindow.currentController {
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.allowsEditing = true
                currentController.present(imagePicker, animated: true, completion: nil)
            }
        } else {
            pickMediaFromGallery(galleryBlock: { (info: [String : Any], pickedImage: UIImage?) in
                cameraBlock(info, pickedImage)
            })
        }
    }
    
    func pickMediaFromGallery(galleryBlock: @escaping DidFinishPickingMediaBlock) {
        
        if let currentController = UIWindow.currentController {
            finishedPickingMediaWithInfo = galleryBlock
            let imagePicker:UIImagePickerController = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            currentController.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK:- - image picker delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if let finishedPickingMediaWithInfo = finishedPickingMediaWithInfo {
            finishedPickingMediaWithInfo(info, image)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
        if let cancelledPickingMediaBlock = cancelledPickingMediaBlock {
            cancelledPickingMediaBlock()
        }
    }
    
}
